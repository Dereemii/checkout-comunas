//////////  start  function to remove the department from the selector  ////////////////
      
$(function () {
    init();
});

function init() {
    comunasOut();
}

function comunasOut() {

    /* start list of communes that should not be displayed */

    var comunas = ["Centro de Ski - El Colorado___7690002",/* RM - Stgo */
        "Centro de Ski - Farellones___7690003",
        "Centro de Ski - La Parva___7690004",
        "Centro de Ski - La Parva___7690004",
        "Centro de Ski - Valle Nevado___7690001",
        "Chicureo___9350001",
        "Lo Barnechea (Excepto Cordillera)___7690100",
        "Caleta Hornos___1780008",/* Comienzo region IV */
        "Guanaqueros___1780007",
        "Las Tacas___1780002",
        "Morrillos___1780004",
        "Playa Blanca___1780003",
        "Puerto Velero___1780001",
        "Tongoy___1780006",
        "Totoralillo___1780005",
        "Lago Rapel___3030001" /* Región VI - O higgins*/,
        "Recinto___3880001", /*  Region VIII - Bio bio */
        "Pingueral___4160002",
        "Lirquen___4140001", 
        "Dichato___4160001",
        "Lago Cólico___4930002"/* Region IX - Araucania*/,
        "Lago Cólico Norte___4890001",
        "Lago Cólico Sur___4890002",
        "Caburga___4920001",
        "Calafquen___4930001",
        "Malalcahuello___4780001",
        "Licanray___4930001",
        "El Islote___4930001",/* Región X - Los Lagos */
        "Ensenada___5550001",
        "Marina Rupanco___5360001",
        "Coñaripe___5210001",/* Region XIV - Los Rios */
    ];

    /* end list of communes that should not be displayed */

    setInterval(function () {

        $("select#ship-state").attr('style',"display: block;");
        $("select#ship-neighborhood").attr('style',"display: block;");

        $("#ship-neighborhood option").each(function () {
            if ($.inArray($(this).val(), comunas) > 0) {
                if($(this).attr('disabled') != 'disabled'){
                    $(this).hide();
                    $(this).attr('disabled', 'disabled');
                }
            }

            if ($(this).val() == 'Centro de Ski - El Colorado___7690002') {
                if($(this).attr('disabled') != 'disabled'){
                    $(this).hide();
                    $(this).attr('disabled', 'disabled');
                }
            }
        });

        $("#ship-state").bind("change", function () {

            setTimeout(function () {

                $("#ship-neighborhood option").each(function () {
                    if ($.inArray($(this).val(), comunas) > 0) {
                        if($(this).attr('disabled') != 'disabled'){
                            $(this).hide();
                            $(this).attr('disabled', 'disabled');
                        }
                    }

                    if ($(this).val() == 'Centro de Ski - El Colorado___7690002') {
                        if($(this).attr('disabled') != 'disabled'){
                            $(this).hide();
                            $(this).attr('disabled', 'disabled');
                        }
                    }
                });
            }, 300);
        });
    }, 2500);
}

  
//////////  end  function to remove the department from the selector  ////////////////
