# Codigo que deshabilita comunas.

Existe un listado de ciudades y localidades que están registradas como comunas desde la base de datos por lo que arroja error si los clientes intentan comprar con despacho a estas.

En el siguiente repositorio encontrarás el listado de comunas entregados por vtex junto a sus códigos postales.

Lo que permite es que se oculten y deshabiliten ciertos ```<option></option>``` para que el cliente no pueda seleccionarlos.

A continuación se muestra cómo actua este código para el cliente, ejemplo: ocultar localidad de "LIRQUEN".

**Nota importante: Considerar que en iphone solo se mostrará deshabilitado cada option pero no se puede ocultar.**

## Vista en Desktop

![Vista desktop](https://gitlab.com/Dereemii/checkout-comunas/-/raw/master/fotos/desktop_error.JPG)

## Vista en Mobile

![Vista mobile](https://gitlab.com/Dereemii/checkout-comunas/-/raw/master/fotos/iphone_mobile.JPG)




15-03-2021 | CMJP